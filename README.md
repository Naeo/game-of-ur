# Game of Ur: the Lua Version!

## LICENSE

This project is licensed under GPLv2.

## Requirements
- lcurses (this means you'll probably need to be on Linux, Mac, or using Windows Subsystem for Linux)
- luajit

## How-to

Download the code.  Simply cd into the directory and run `luajit curses_interface.lua` and the game will start!

## Bugs and stuff

A few outstanding bugs:

- I once observed a capture from the third rosette to the last shared space that just, uh, didn't work.  I might or might not bother to fix this.

## But what are the rules?

I couldn't be bothered to include the rules, so consult the [Wikipedia page](https://en.wikipedia.org/wiki/Royal_Game_of_Ur) or, better yet, the [Irving Finkel and Tom Scott YouTube video](https://www.youtube.com/watch?v=WZskjLq040I).
