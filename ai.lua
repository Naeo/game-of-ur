--[[    
    The functions in this program are built around the AI player.
    
    TODO: refactor to accept GAME_STATE table rather than individual
    piece location tables.
]]

local board = require("board")
local util = require("util")

local ai = {}

ai.roll_probabilities = {
    [0] = 1/16,
    [1] = 4/16,
    [2] = 6/16,
    [3] = 4/16,
    [4] = 1/16
}

function ai.aggregate_move_tree(probability_tree, player, strategy)
    -- Collapse the probability tree returned by ai.recursively_evaluate_moves()
    -- such that all that remains is a single score for the entire tree.
    -- Sensible options: expected value of terminal nodes; max of terminal nodes

    -- Expected value: prob = product of all probabilities higher in the tree,
    -- score = prob * computer_goodness of the state

    -- Max: Just pick the max score of any terminal node

    if strategy == nil then strategy = "max" end

    local scores = {}
    for k,v in ipairs(probability_tree) do
        local current_score
        if type(v) ~= "table" then
            -- do nothing
        elseif v.future_states ~= nil then
            scores[#scores+1] = ai.aggregate_move_tree(
                v.future_states, player, strategy
            )
        elseif strategy == "max" then
            scores[#scores+1] = v.game_state[player .. "_goodness"]
        elseif strategy == "ev" then
            scores[#scores+1] = v.game_state[player .. "_goodness"] * v.probability
        end
    end

    if #scores == 0 then
        return 0
    else
        return util.max(scores)
    end
end

function ai.recursively_evaluate_moves(state, player, depth)
    -- Edge case for when to stop recursion
    if depth <= 0 then return nil end
    if state.player_score == 7 or state.computer_score == 7 then
        return nil
    end

    --print("DEBUG player=" .. player .. ", depth=" .. depth)

    -- TODO: might need some logic to catch when there are no valid moves in a branch.
    -- TODO: this doesn't seem to be working with rolls of zero.

    local probability_tree = {}
    for roll=0, 4 do
        local prob = ai.roll_probabilities[roll]
        
        for k,v in ipairs(state[player .. "_positions"]) do
            local move, message = board.move_piece(
                state, k, k+roll, player
            )
            --print(string.format(
                --"DEBUG message=%s, roll=%s, prob=%s, depth=%s, player=%s, from=%s",
                --message, roll, prob, depth, player, k
            --))
            if message == "rosette" then
                probability_tree[#probability_tree + 1] = {
                    probability=prob,
                    game_state=move,
                    depth=depth,
                    future_states=ai.recursively_evaluate_moves(move, player, depth)
                }
            else
                probability_tree[#probability_tree + 1] = {
                    probability=prob,
                    game_state=move,
                    future_states=ai.recursively_evaluate_moves(
                        move, util.other_player(player), depth - 1
                    )
                }
            end
        end
    end

    return probability_tree
end

-- TODO: clean this up.  Given a set of possible moves, evaluate them
-- appropriately, i.e., using the recursive evaluation above and dealing
-- with rosettes.
function ai.select_move(state, roll, player, strategy)
    -- Calculate all possible moves given the current board state
    -- and the current roll value.  Return the board state after
    -- making each possible move.

    -- Random strategy is a special case, so take care of it sooner
    -- rather than later.
    if strategy == "random" then
        local possible_moves = {}
        local move_candidate
        local move_message
        for k,v in pairs(state[player .. "_positions"]) do
            move_candidate, move_message = board.move_piece(
                state, k, k+roll, player
            )
            if move_message ~= "illegal" then
                possible_moves[#possible_moves + 1] = {move_candidate, move_message}
            end
        end

        if #possible_moves == 0 then
            possible_moves = {{util.deepcopy(state), "", 0}}
        end

        return possible_moves[math.random(#possible_moves)]
    end
        
    
    -- Iterate through all possible pieces on the board and move them.
    local move_candidates = {}
    local candidate_move_score
    local move_message
    local next_move

    if strategy == nil then local strategy = "max" end

    for k,v in pairs(state[player .. "_positions"]) do
        next_move, move_message = board.move_piece(state, k, k+roll, player)
        if move_message == "" then
            candidate_move_score = ai.recursively_evaluate_moves(
                next_move,
                util.other_player(player),
                3
            )
        elseif move_message == "rosette" then
            candidate_move_score = ai.recursively_evaluate_moves(
                next_move,
                player,
                3
            )
        end

        if next_move[player .. "_score"] == 7 then
            move_candidates[#move_candidates+1] = {
                next_move,
                move_message,
                1000
            }
        elseif next_move[util.other_player(player) .. "_score"] == 7 then
            move_candidates[#move_candidates+1] = {
                next_move,
                move_message,
                -1000
            }
        elseif move_message ~= "illegal" then
            candidate_move_score = ai.aggregate_move_tree(
                candidate_move_score,
                player,
                strategy
            )

            move_candidates[#move_candidates+1] = {
                next_move,
                move_message,
                candidate_move_score
            }
        end
    end

    -- Randomly select from the move(s) with the highest score.
    local selected_moves = {}
    local max_score
    for k,v in ipairs(move_candidates) do
        if max_score == nil then
            max_score = v[3]
        elseif v[3] >= max_score then
            max_score = v[3]
        end
    end
    for k,v in ipairs(move_candidates) do
        if v[3] == max_score then
            selected_moves[#selected_moves + 1] = v
        end
    end

    -- If we have no legal moves, we can only choose the current state
    -- to return.
    if #selected_moves == 0 then
        selected_moves = {{util.deepcopy(state), "", 0}}
    end

    return selected_moves[math.random(#selected_moves)]
end

return ai
