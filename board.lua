--[[
-- Board-related functions:
--
-- board.move_piece(state, from, to, player)
--  - Library function meant to be called externally.
--
-- board.score_board(state)
--  - Internal function not meant to be called externally.
]]
local util = require("util")

local board = {}

function board.score_board(state)
    -- Add the human/computer goodness scores to the board using
    -- the human/computer scoring weights.

    -- Quick check: if a player wins, set an absurdly large value.
    if state.computer_score == 7 then
        state.computer_goodness = 1000
        state.human_goodness    = -1000
        return state
    elseif state.human_score == 7 then
        state.computer_goodness = -1000
        state.human_goodness    = 1000
        return state
    end

    local densified = {
        state.computer_score,
        state.computer_reserve,
        state.computer_positions[2]  == nil and 0 or 1,
        state.computer_positions[3]  == nil and 0 or 1,
        state.computer_positions[4]  == nil and 0 or 1,
        state.computer_positions[5]  == nil and 0 or 1,
        state.computer_positions[6]  == nil and 0 or 1,
        state.computer_positions[7]  == nil and 0 or 1,
        state.computer_positions[8]  == nil and 0 or 1,
        state.computer_positions[9]  == nil and 0 or 1,
        state.computer_positions[10] == nil and 0 or 1,
        state.computer_positions[11] == nil and 0 or 1,
        state.computer_positions[12] == nil and 0 or 1,
        state.computer_positions[13] == nil and 0 or 1,
        state.computer_positions[14] == nil and 0 or 1,
        state.computer_positions[15] == nil and 0 or 1,
        
        state.human_score,
        state.human_reserve,
        state.human_positions[2]  == nil and 0 or 1,
        state.human_positions[3]  == nil and 0 or 1,
        state.human_positions[4]  == nil and 0 or 1,
        state.human_positions[5]  == nil and 0 or 1,
        state.human_positions[6]  == nil and 0 or 1,
        state.human_positions[7]  == nil and 0 or 1,
        state.human_positions[8]  == nil and 0 or 1,
        state.human_positions[9]  == nil and 0 or 1,
        state.human_positions[10] == nil and 0 or 1,
        state.human_positions[11] == nil and 0 or 1,
        state.human_positions[12] == nil and 0 or 1,
        state.human_positions[13] == nil and 0 or 1,
        state.human_positions[14] == nil and 0 or 1,
        state.human_positions[15] == nil and 0 or 1,

        -- Intercept
        1
    }

    local computer_goodness = 0
    local human_goodness    = 0
    for i=1, #densified do
        computer_goodness = computer_goodness + (densified[i] * state.computer_weights[i])
        human_goodness = human_goodness + (densified[i] * state.human_weights[i])
    end
    state.computer_goodness = computer_goodness
    state.human_goodness    = human_goodness

    return state
    
end

function board.move_piece(state, from, to, player)
    local other  = util.other_player(player)
    local state_ = util.deepcopy(state)

    if (
        from == to
        or (from > 1 and state[player .. "_positions"][from] == nil)
    ) then
        board.score_board(state_)
        return state_, "illegal"
    end

    if to == 16 then
        state_[player .. "_score"] = state_[player .. "_score"] + 1
        state_[player .. "_positions"][16] = nil
        state_[player .. "_positions"][from] = nil
        board.score_board(state_)
        return state_, ""
    end

    --[[
        Impossible moves:
        - Land on your own piece
        - Land on other player's piece on the rosette
        - Move past the end of the board
        - Move piece from an empty space
    ]]
    if (
        state_[player .. "_positions"][to] == true
        or (to == 9 and state_[other .. "_positions"][to] == true)
        or (from == 1 and state_[player .. "_reserve"] <= 0)
        or state_[player .. "_positions"][from] == nil
        or to > 16
    ) then
        return state_, "illegal"
    end

    -- Move the piece
    state_[player .. "_positions"][to]   = true
    state_[player .. "_positions"][from] = nil

    -- Capture opponent's piece, if applicable.
    if to > 5 and to < 13 then
        if state_[other .. "_positions"][to] == true then
            state_[other .. "_positions"][to] = nil
            state_[other .. "_reserve"]       = state_[other .. "_reserve"] + 1
            -- Capture --> always results in piece available to move --> always
            -- puts a piece back on position 1.
            state_[other .. "_positions"][1]  = true
        end
    end

    -- Deal with reserve pieces.
    if from == 1 then
        state_[player .. "_reserve"] = state_[player .. "_reserve"] - 1
        if state_[player .. "_reserve"] <= 0 then
            state_[player .. "_positions"][1] = nil
        else
            state_[player .. "_positions"][1] = true
        end
    end

    -- Return a rosette message if player landed on a rosette.
    board.score_board(state_)
    if to == 5 or to == 9 or to == 15 then
        return state_, "rosette"
    else
        return state_, ""
    end
end


return board
