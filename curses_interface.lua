local ai     = require("ai")
local bit    = require("bit")
local board  = require("board")
local curses = require("curses")
local util   = require("util")

--[[
    curses interface for the game.

    TODO:
        - This file is mostly done--just need to integrate it into main.lua

        - Vertically center the board above the scoreboard.
        - Add a single space of padding to all output on all sides.
]]

-- Set the random seed
math.randomseed(util.get_random_seed())

--local curses = {}

local best_weights = {
    {2.8322869898706,1.8175272009966,-1.9741714134858,-1.667968913877,-2.0022733179072,-2.4347121793575,0.55720558892021,1.6935361335315,-2.4796857770514,-1.082795936855,-2.3498684244024,0.18457034615289,-1.7431711154029,-0.90121244387768,1.8040219181503,2.9176453688676,1.1348730018678,1.9331474474286,-1.333738031904,0.38125321934117,0.88358262484039,0.57816811150724,-0.25302342862606,-1.8595267774022,0.40351940921675,-0.4366879553177,-1.5963945524987,1.2110374067221,-0.94755150470885,-0.054391907922173,0.28079796298238,-2.0089421877688,-1.9291885671706},
    {2.2689146821998,0.90543279828844,2.783990188294,-1.2495934787106,2.2340950496772,1.2858711170067,2.4907097463607,0.90456573598699,0.39852630195503,0.62125124360528,1.7283841974661,-1.6878493625635,0.18427827541177,-0.60242491668446,1.8298901825497,-2.7731129021667,1.009442630897,2.1079724635757,0.21100929703017,-0.50599422609077,2.6542921816759,-1.043248880889,2.3230750902024,-0.75819866946949,-1.7439383510463,0.84125012524153,-1.2010369949859,-2.0929573377263,-2.1968983742021,1.03307558382,-0.00736589283028,-0.77268653475286,-1.3640509528724},
    {1.1978394054563,-0.86816132200145,1.3423400517682,-0.71454667678232,1.8959411619884,2.0952939092788,-1.3398595328008,-1.5108453525592,-2.2166571232287,0.7666129608315,-1.3206942173481,-2.5232475574935,-1.0418558931921,2.8262420696659,2.8567081573198,2.1800921639714,0.39641898963682,2.487258294233,1.5570524162699,0.5919560818218,2.9208007708399,-0.23815976869236,-1.0186126237271,-2.6933593640577,-1.7599687578158,-1.0931909172923,-1.9018706220283,-1.7350863319101,0.23322147397041,-0.26030279656191,2.6302160698625,-0.96695525434837,-1.4790469700111},
    -- {3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0},
}

function init_constants()
    STATE = {
        human_positions    = {[1]=true}, -- Piece positions, {position=true}
        human_score        = 0,  -- In-game score/pieces moved off board
        human_reserve      = 7,  -- Pieces not yet on board
        human_goodness     = 0,  -- Goodness metric for human player
        human_weights      = {   -- Default scoring weights
            -3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
             3,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
             0
        }, 
        computer_positions = {[1]=true},
        computer_score     = 0,
        computer_reserve   = 7,
        computer_goodness  = 0,
        computer_weights   = best_weights[math.random(#best_weights)] 
    }

    if curses.has_colors() then
        curses.start_color()
        curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_BLUE,  curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_RED,   curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_WHITE,   curses.COLOR_BLACK)
    end

    -- Basic format constants
    GREEN  = curses.color_pair(1)
    BLUE   = curses.color_pair(2)
    RED    = curses.color_pair(3)
    NORMAL = curses.color_pair(4)
    FLASH  = bit.bor(curses.A_BLINK, curses.A_REVERSE)

    FLASH_GREEN = bit.bor(
        curses.A_BLINK, curses.A_REVERSE, curses.color_pair(1)
    )
    
    FLASH_BLUE = bit.bor(
        curses.A_BLINK, curses.A_REVERSE, curses.color_pair(2)
    )
    
    FLASH_RED = bit.bor(
        curses.A_BLINK, curses.A_REVERSE, curses.color_pair(3)
    )

    -- Message constants
    ILLEGAL_MOVE = curses.chstr(20)
    ILLEGAL_MOVE:set_str(
        0,
        "ERROR: Invalid move.",
        FLASH_RED
    )
    
    ROSETTE = curses.chstr(37)
    ROSETTE:set_str(
        0,
        "You landed on a rosette!  Move again.",
        FLASH_GREEN
    )

    ZERO_ROLL = curses.chstr(50)
    ZERO_ROLL:set_str(
        0,
        "Oh no, you rolled a 0!  You can't do anything.  :(",
        FLASH_RED
    )

    NO_MOVES = curses.chstr(36)
    NO_MOVES:set_str(
        0,
        "Oh no, you can't make any moves!  :(",
        FLASH_RED
    )

    YOU_WIN = curses.chstr(50)
    YOU_WIN:set_str(
        0,
        "You win! :D  Press enter to play again, q to quit.",
        FLASH_GREEN
    )
    
    YOU_LOSE = curses.chstr(51)
    YOU_LOSE:set_str(
        0,
        "You lose! :(  Press enter to play again, q to quit.",
        FLASH_RED
    )

    CONTROLS = {
        "Controls:",
        "  n      - Add new piece to the board",
        "  q      - Quit",
        "  arrows - Move cursor",
        "  enter  - Move selected piece;",
        "           confirm/clear messages"
    }

end

function make_move(x, y, roll, state)
    -- Get the current piece position at the cursor and try to make the move.

    local human_position
    if y == 0 then
        if     x == 0  then human_position = 5
        elseif x == 2  then human_position = 4
        elseif x == 4  then human_position = 3
        elseif x == 6  then human_position = 2
        elseif x == 8  then human_position = 1 -- special case to add new piece
        elseif x == 12 then human_position = 15
        elseif x == 14 then human_position = 14
        else                human_position = -1
        end
    elseif y == 1 then
        if     x == 0  then human_position = 6
        elseif x == 2  then human_position = 7
        elseif x == 4  then human_position = 8
        elseif x == 6  then human_position = 9
        elseif x == 8  then human_position = 10
        elseif x == 10 then human_position = 11
        elseif x == 12 then human_position = 12
        elseif x == 14 then human_position = 13
        else                human_position = -1
        end
    else
        human_position = -1
    end

    local state, msg = board.move_piece(
        state,
        human_position,
        human_position + roll,
        "human"
    )

    return state, msg
end

function print_board(state, roll, player)
    local function human(loc)
        if state.human_positions[loc] ==true then
            return {"H", GREEN}
        elseif loc == 5 or loc == 9 or loc == 15 then
            return {"*", BLUE}
        else
            return {"-", NORMAL}
        end
    end

    local function computer(loc)
        if state.computer_positions[loc] == true then
            return {"C", RED}
        elseif loc == 5 or loc == 9 or loc == 15 then
            return {"*", BLUE}
        else
            return {"-", NORMAL}
        end
    end

    local function shared(loc)
        if human(loc)[1] == "H" then
            return human(loc)
        else
            return computer(loc)
        end
    end

    local printable = {
        curses.chstr(15),
        curses.chstr(15),
        curses.chstr(15)
    }

    printable[1]:set_str(0,  human(5)[1],  human(5)[2])
    printable[1]:set_str(2,  human(4)[1],  human(4)[2])
    printable[1]:set_str(4,  human(3)[1],  human(3)[2])
    printable[1]:set_str(6,  human(2)[1],  human(2)[2])
    printable[1]:set_str(12, human(15)[1], human(15)[2])
    printable[1]:set_str(14, human(14)[1], human(14)[2])
    
    printable[2]:set_str(0,  shared(6)[1],  shared(6)[2])
    printable[2]:set_str(2,  shared(7)[1],  shared(7)[2])
    printable[2]:set_str(4,  shared(8)[1],  shared(8)[2])
    printable[2]:set_str(6,  shared(9)[1],  shared(9)[2])
    printable[2]:set_str(8,  shared(10)[1], shared(10)[2])
    printable[2]:set_str(10, shared(11)[1], shared(11)[2])
    printable[2]:set_str(12, shared(12)[1], shared(12)[2])
    printable[2]:set_str(14, shared(13)[1], shared(13)[2])

    printable[3]:set_str(0,  computer(5)[1],  computer(5)[2])
    printable[3]:set_str(2,  computer(4)[1],  computer(4)[2])
    printable[3]:set_str(4,  computer(3)[1],  computer(3)[2])
    printable[3]:set_str(6,  computer(2)[1],  computer(2)[2])
    printable[3]:set_str(12, computer(15)[1], computer(15)[2])
    printable[3]:set_str(14, computer(14)[1], computer(14)[2])

    -- Print board contents
    stdscr:mvaddchstr(0, 0, printable[1])
    stdscr:mvaddchstr(1, 0, printable[2])
    stdscr:mvaddchstr(2, 0, printable[3])

    -- Print roll/current player
    stdscr:move(4, 0)
    stdscr:clrtoeol()
    if player == "human" then
        stdscr:mvaddstr(4, 0, "It's your turn!")
    else
        stdscr:move(4, 0)
        stdscr:clrtoeol()
        stdscr:mvaddstr(4, 0, "It's the computer's turn!")
    end
    stdscr:mvaddstr(5, 0, string.format("Roll: %d", roll))

    -- Scoreboard
    stdscr:mvaddstr(8,  0, "+-----------------++-----------------+")
    stdscr:mvaddstr(9,  0, "|       You       ||    Computer     |")
    stdscr:mvaddstr(10, 0, "+-----------------++-----------------+")
    stdscr:mvaddstr(11, 0, "| Score | Reserve || Score | Reserve |")
    stdscr:mvaddstr(12, 0, "+-------+---------++-------+---------+")
    stdscr:mvaddstr(13, 0,
        string.format(
            "|   %d   |    %d    ||   %d   |    %d    |",
            state.human_score,
            state.human_reserve,
            state.computer_score,
            state.computer_reserve
        )
    )
    stdscr:mvaddstr(14, 0, "+-------+---------++-------+---------+")

    -- Controls
    for k,v in ipairs(CONTROLS) do
        stdscr:mvaddstr(k + 15, 0, v)
    end

    -- Reset cursor and draw screen
    stdscr:move(y, x)
    stdscr:refresh()
end

function get_user_input(stdscr, roll, state)
    -- If the roll is 0, basically do nothing
    if roll == 0 then
        stdscr:move(6, 0)
        stdscr:clrtoeol()
        stdscr:mvaddchstr(6, 0, ZERO_ROLL)
        stdscr:refresh()
        stdscr:move(y, x)
        local c = stdscr:getch()
        while c ~= 13 do c = stdscr:getch() end
        stdscr:move(6, 0)
        stdscr:clrtoeol()
        stdscr:move(y, x)
        return state, "zero"
    end

    -- Else, check that the user has any valid moves.
    -- If not, return.
    local valid_moves = 0
    local attempted_move, attempted_move_msg
    for k, v in pairs(state.human_positions) do
        attempted_move, attempted_move_msg = board.move_piece(
            state,
            k,
            k+roll,
            "human"
        )
        if attempted_move_msg ~= "illegal" then
            valid_moves = valid_moves + 1
        end
    end
    if valid_moves == 0 then
        stdscr:move(6, 0)
        stdscr:clrtoeol()
        stdscr:mvaddchstr(6, 0, NO_MOVES)
        stdscr:refresh()
        stdscr:move(y, x)
        local c = stdscr:getch()
        while c ~= 13 do c = stdscr:getch() end
        stdscr:move(6, 0)
        stdscr:clrtoeol()
        stdscr:move(y, x)
        return state, "no moves"
    end
        
    while true do
        local c = stdscr:getch()
        if c < 256 then
            if string.char(c) == "q" then
                stdscr:endwin()
                os.exit() 
            elseif c == 13 then -- newline
                STATE, msg = make_move(x, y, roll, state)
                print_board(STATE, roll, "human")
                stdscr:move(6, 0)
                stdscr:clrtoeol()
                if msg == "illegal" then
                    stdscr:mvaddchstr(6, 0, ILLEGAL_MOVE)
                elseif msg == "rosette" then
                    stdscr:mvaddchstr(6, 0, ROSETTE)
                else
                    stdscr:addstr(msg)
                end
                stdscr:move(y, x)
                stdscr:refresh()
                if msg ~= "illegal" then break end
            elseif string.char(c) == "n" then
                STATE, msg = make_move(8, 0, roll, state)
                print_board(STATE, roll, "human")
                stdscr:move(6, 0)
                stdscr:clrtoeol()
                if msg == "illegal" then
                    stdscr:mvaddchstr(6, 0, ILLEGAL_MOVE)
                elseif msg == "rosette" then
                    stdscr:mvaddchstr(6, 0, ROSETTE)
                else
                    stdscr:addstr(msg)
                end
                stdscr:move(y, x)
                stdscr:refresh()
                if msg ~= "illegal" then break end
            end
        elseif c == curses.KEY_UP then
            if x <= 6 or x >= 12 then
                y = math.max(0, y - 1)
            end
            stdscr:move(y, x)
            stdscr:refresh()
        elseif c == curses.KEY_DOWN then
            if x <= 6 or x >= 12 then
                y = math.min(3, y + 1)
            end
            stdscr:move(y, x)
            stdscr:refresh()
        elseif c == curses.KEY_LEFT then
            if x == 12 and y ~= 1 then
                x = math.max(0, x - 6)
            else
                x = math.max(0, x - 2)
            end
            stdscr:move(y, x)
            stdscr:refresh()
        elseif c == curses.KEY_RIGHT then
            if x == 6 and y ~= 1 then
                x = math.min(14, x + 6)
            else
                x = math.min(14, x + 2)
            end
            stdscr:move(y, x)
            stdscr:refresh()
        end
    end

    return STATE, msg
end

function main()
    stdscr = curses.initscr()

    init_constants()
    
    curses.cbreak()
    curses.echo(false)
    curses.nl(false)
    stdscr:keypad(true)
    stdscr:clear()

    -- Position of the cursor.  x must be 0-indexed, because curses uses C-style
    -- 0-indexing.
    x = 6
    y = 0

    local player = math.random() < 0.5 and "human" or "computer"
    
    while STATE.human_score < 7 and STATE.computer_score < 7 do
        roll = util.roll_dice()
        print_board(STATE, roll, player)
        stdscr:move(y, x)
        if player == "human" then
            STATE, msg = get_user_input(stdscr, roll, STATE)
            print_board(STATE, roll, player)
        else
            print_board(STATE, roll, player)
            util.sleep(1)
            moves = ai.select_move(STATE, roll, "computer", "ev")
            STATE = moves[1]
            msg   = moves[2]
            print_board(STATE, roll, player)
        end

        if msg ~= "rosette" then player = util.other_player(player) end        
    end

    stdscr:move(3, 0)
    stdscr:clrtoeol()
    if STATE.human_score == 7 then
        stdscr:mvaddchstr(3, 0, YOU_WIN)
    else
        stdscr:mvaddchstr(3, 0, YOU_LOSE)
    end
    stdscr:move(y, x)

    local play_again
    while true do
        local play_again = stdscr:getch()
        if play_again == 13 or play_again == 113 then break end
    end

    curses.endwin()
    return play_again
end

-- To display Lua errors, we must close curses to return to
-- normal terminal mode, and then write the error to stdout.
local function err(err)
  curses.endwin()
  print("Caught an error:")
  print(debug.traceback (err, 2))
  os.exit(2)
end

play_again = 13
while (play_again == 13) or (play_again == 113) do
    play_again = xpcall(main, err)
end
