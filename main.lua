--[[
    TODO LIST
        - Integrate curses here.
        - Rewrite curses interface to be trivially interchangeable with a LOVE one.
        - General refactor.
            - Move this current form of the file to its own "simulation" file.
            - Rewrite this mian file using the new curses interface.  Make sure the
              curses interface and the LOVE interface are drop-in replacements for
              each other based on whether LOVE is available or not.
]]

-- standard library
math  = require("math")

-- third party libraries
cjson = require("cjson")

-- custom modules
ai    = require("ai")
board = require("board")
util  = require("util")

-- Random seed from current time
local seed = util.get_random_seed()
math.randomseed(seed)

-- The game states will be sparse arrays, so have cjson convert them when
-- json-serializing.
cjson.encode_sparse_array(true)

-- The blank/starting game state.
STATE = {
    human_positions    = {[1]=true}, -- Piece positions, {position=true}
    human_score        = 0,          -- In-game score/pieces moved off board
    human_reserve      = 7,          -- Pieces not yet on board
    human_goodness     = 0,          -- Goodness metric for human player
    human_weights      = {           -- Default scoring weights
        -3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
         3,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
         0
    }, 
    computer_positions = {[1]=true},
    computer_score     = 0,
    computer_reserve   = 7,
    computer_goodness  = 0,
    computer_weights   = {
         3,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
        -3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
         0
    }
}

local game_log   = io.open(arg[1] ~= nil and arg[1] or "games/GAMES.log",   "a")
for GAME=1, 1000 do
    --local human_weights    = util.init_random_weights()
    local computer_weights = util.init_random_weights()

    local human_strat      = "ev"
    local computer_strat   = "ev"
    local SCORES = {human=0, computer=0, weights=computer_weights}

    for MATCH=1, 20 do
        local game_message = "START\n"
        local state            = util.deepcopy(STATE)
        local player           = math.random() < 0.5 and "human" or "computer"

        state.computer_weights = computer_weights

        while state.human_score < 7 and state.computer_score < 7 do        
            local roll = util.roll_dice()
            if player == "computer" then
                moves = ai.select_move(
                    state, roll, player, computer_strat
                )
            elseif player == "human" then
                moves = ai.select_move(
                    state, roll, player, human_strat
                )
            end
            state = util.deepcopy(moves[1])
            local msg = moves[2]

            --print(roll)
            --print(player)
            --print(util.table_to_string(moves))
            --util.print_board(state)
            --print("Computer: " .. state.computer_score)
            --print("Human:    " .. state.human_score)
            --print()
            
            if msg ~= "rosette" then
                player = util.other_player(player)
            end

            game_message = game_message .. "STATE " .. cjson.encode(state) .. "\n"
            
            if state.human_score == 7 then
                SCORES.human = SCORES.human + 1
                game_message = game_message .. "FINAL " .. cjson.encode({
                    computer_strat=computer_strat,
                    human_strat=human_strat,
                    computer_weights=state.computer_weights,
                    human_weights=state.human_weights,
                    human_score=state.human_score,
                    computer_score=state.computer_score,
                    winner="human"
                }) .. "\n"
            elseif state.computer_score == 7 then
                SCORES.computer = SCORES.computer + 1
                game_message = game_message .. "FINAL " .. cjson.encode({
                    computer_strat=computer_strat,
                    human_strat=human_strat,
                    computer_weights=state.computer_weights,
                    human_weights=state.human_weights,
                    human_score=state.human_score,
                    computer_score=state.computer_score,
                    winner="computer"
                }) .. "\n"
            end
        end
        --game_log:write(game_message)
    end

    --print(SCORES.human, SCORES.computer)
    local scores_msg = "WEIGHT_RESULTS " .. cjson.encode(SCORES) .. "\n"
    game_log:write(scores_msg)

end

game_log:close()
