# Refactor notes

## Project Structure
### `main.lua`
This is the main file.  It should contain minimal logic.  It should only call library functions.

This is the program that should be directly run by the end user to play the game.

Since there will be an ncurses and a LOVE interface, there may need to be some basic logic along the following lines:

```
if love_is_available then
    love_main()
else
    ncurses_main()
end
```

### `board.lua`

This contains general utilities for interacting with the board/game state: moving pieces, doing scores, etc.

### `ai.lua`

This contains functions for the game's AI: 2-ply forward-looking move selection takes up the vast majority of the code here.

### `util.lua`

Contains miscellaneous, usually small, utility functions.

### `ncurses_interface.lua`

Contains the implementation of the ncurses interface for the game.

### `love.lua` -- TODO

Contains the implementation of the LOVE interface for the game, which will be the same as the ncurses one, but designed to work cross-platform with the LOVE framework.

This should have the same interface as the `ncurses_interface.lua` file to simplify the primary call in `main.lua`.


## Refactor steps

- Remove extraneous and unused functions.
- Simplify code and profile to make sure it didn't break.
- Find anything that will require cross-platform tweaking.
    - util.get_random_seed() calls /dev/urandom, will break on Windows unless using WSL
