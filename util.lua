--[[
-- Miscellaneous utility functions, meant for use all over
-- the program.
]]

local util = {}

function util.sleep(n)
    os.execute("sleep " .. tonumber(n))
end

function util.init_random_weights()
    -- Initialize random weights for the scoring function.
    local weights = {}
    for i=1, 33 do
        weights[#weights + 1] = (math.random() * 6) - 3
    end
    return weights
end

function util.get_random_seed()
    -- Generate a random seed from /dev/urandom
    local random_data = io.open("/dev/urandom", "rb"):read(32)
    local seed = 4
    for i=1, random_data:len() do
        seed = seed*256 + random_data:byte(i)
    end
    return seed
end

function util.max(tbl)
    -- Gets the max value from a table of simple {k=v} pairs.
    local maxval
    for k,v in ipairs(tbl) do
        if maxval == nil then
            maxval = v
        elseif v >= maxval then
            maxval = v
        end
    end
    return maxval
end

function util.roll_dice()
    -- Simulate a roll of the dice and return the value.
    
    -- Possible total values, each repeated the number of times
    -- that they can occur on the four tetrahedral dice.
    local possible_rolls = {
        0, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4
    }
    local roll = math.random(#possible_rolls)
    
    return possible_rolls[roll]
end

function util.other_player(player)
    -- The "switch to the other player" logic is so commonly used
    -- that it deserves its own function.
    if player == "human" then
        return "computer"
    else
        return "human"
    end
end

function util.table_to_string(tbl, indent)
    -- General utility function mostly for debugging.
    -- Pretty-prints the contents of a table, recursing into
    -- any keys/values that are themselves tables.
    if type(tbl) ~= "table" then return tostring(tbl) end

    local indent_ = ""
    if indent ~= nil then
        indent_ = indent
    end
    
    local formatted = "{\n"
    for k,v in pairs(tbl) do
        formatted = formatted .. string.format(
            "%s\t%s = %s,\n",
            indent_,
            util.table_to_string(k, "\t" .. indent_),
            util.table_to_string(v, "\t" .. indent_)
        )
    end
    formatted = formatted .. indent_ .. "}"

    return formatted
end

function util.deepcopy(orig)
    -- Copy a table by value, not reference.  This will only be called
    -- on the game state, so we can custom-tailor it for that structure.
    -- This is one of the most-called functions in the program due to the
    -- recursive search the AI does for moves, so this optimization is
    -- critical for performance.
    local copy = {
        human_positions    = {},
        human_score        = orig.human_score,
        human_reserve      = orig.human_reserve,
        human_goodness     = orig.human_goodness,
        human_weights      = {}, 
        computer_positions = {},
        computer_score     = orig.computer_score,
        computer_reserve   = orig.computer_reserve,
        computer_goodness  = orig.computer_goodness,
        computer_weights   = {}
    }

    -- Weight vectors are always the same length --> save a bit of
    -- time by iterating like this.
    for i=1, #orig.human_weights do
        copy.human_weights[i]    = orig.human_weights[i]
        copy.computer_weights[i] = orig.computer_weights[i]
    end

    -- Still have to iterate over these tables individually though
    for k,v in pairs(orig.human_positions) do
        copy.human_positions[k] = v
    end

    for k,v in pairs(orig.computer_positions) do
        copy.computer_positions[k] = v
    end

    return copy
    
end

-- Some color functions using ASCII escapes.
function util.green(text)
    -- Use ANSI terminal escapes to print `text` in green.
    return string.char(27) .. '[92m' .. text .. string.char(27) .. '[0m'
end

function util.red(text)
    -- Use ANSI terminal escapes to print `text` in red.
    return string.char(27) .. '[91m' .. text .. string.char(27) .. '[0m'
end

function util.blue(text)
    -- Use ANSI terminal escapes to print `text` in blue.
    return string.char(27) .. '[94m' .. text .. string.char(27) .. '[0m'
end

function util.blink(text)
    -- Use ANSI terminal escapes to print `text` in blinking text.
    return string.char(27) .. '[6m' .. text .. string.char(27) .. '[0m'
end

function util.generate_printable_board(state)
    -- Generate a printable board array as a table of tables.
    -- This is meant to work with normal terminal outputs, and
    -- is primarily for debugging if needed.  Use the curses
    -- interface for interactive sessions.
    local human_token    = util.green(" H ")
    local computer_token = util.red(" C ")
    local empty_token    = " - "
    local empty_rosette  = util.blue(" ❁ ")

    local printable = {
        -- Human safespots
        {
            state.human_positions[5] and human_token or empty_rosette,
            state.human_positions[4] and human_token or empty_token,
            state.human_positions[3] and human_token or empty_token,
            state.human_positions[2] and human_token or empty_token,
            "   ",
            "   ",
            state.human_positions[15] and human_token or empty_rosette,
            state.human_positions[14] and human_token or empty_token,
        },

        -- Shared spaces
        {
            state.computer_positions[6]  and computer_token or (
                state.human_positions[6] and human_token or empty_token
            ),
            state.computer_positions[7]  and computer_token or (
                state.human_positions[7] and human_token or empty_token
            ),
            state.computer_positions[8]  and computer_token or (
                state.human_positions[8] and human_token or empty_token
            ),
            state.computer_positions[9]  and computer_token or (
                state.human_positions[9] and human_token or empty_rosette
            ),
            state.computer_positions[10] and computer_token or (
                state.human_positions[10] and human_token or empty_token
            ),
            state.computer_positions[11] and computer_token or (
                state.human_positions[11] and human_token or empty_token
            ),
            state.computer_positions[12] and computer_token or (
                state.human_positions[12] and human_token or empty_token
            ),
            state.computer_positions[13] and computer_token or (
                state.human_positions[13] and human_token or empty_token
            ),
        },

        -- Computer safespots
        {
            state.computer_positions[5] and computer_token or empty_rosette,
            state.computer_positions[4] and computer_token or empty_token,
            state.computer_positions[3] and computer_token or empty_token,
            state.computer_positions[2] and computer_token or empty_token,
            "   ",
            "   ",
            state.computer_positions[15] and computer_token or empty_rosette,
            state.computer_positions[14] and computer_token or empty_token,
        }
    }

    return printable
end

function util.print_board(state)
    -- Print out a board generated by util.generate_printable_board().
    -- Like generate_printable_board() this is mostly a debugging function
    -- that usually won't need to be called.
    for k, v in ipairs(util.generate_printable_board(state)) do
        print(table.concat(v))
        io.write("\n")
    end
end

return util
